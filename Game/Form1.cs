﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form1 : Form
    {
        private Point StartPosPlayer;
        public Form1()
        {
            InitializeComponent();
            StartPosPlayer.Y = pnlLevel.Height / 2;
            StartPosPlayer.X = pnlLevel.Width / 2;
            World.Instance.player1 = new Player();
            World.Instance.player1.StandardStats(StartPosPlayer);
        }

        private void trUpdare_Tick(object sender, EventArgs e)
        {
            World.Instance.Update();
            if(World.Instance.player1.Draw)
            {
                pnlLevel.Refresh();
            }
            else { }
        }

        private void pnlLevel_Paint(object sender, PaintEventArgs e)
        {
            Graphics gc = e.Graphics;
            World.Instance.Draw(gc);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.A)
            {
                World.Instance.player1.left = true;
            }
            if (e.KeyCode == Keys.S)
            {
                World.Instance.player1.down = true;
            }
            if (e.KeyCode == Keys.D)
            {
                World.Instance.player1.right = true;
            }
            if (e.KeyCode == Keys.W)
            {
                World.Instance.player1.up = true;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                World.Instance.player1.left = false;
            }
            if (e.KeyCode == Keys.S)
            {
                World.Instance.player1.down = false;
            }
            if (e.KeyCode == Keys.D)
            {
                World.Instance.player1.right = false;
            }
            if (e.KeyCode == Keys.W)
            {
                World.Instance.player1.up = false;
            }
        }

        private void pnlLevel_MouseClick(object sender, MouseEventArgs e)
        {
            World.Instance.player1.Shoot(e.Location);
        }
    }
}
