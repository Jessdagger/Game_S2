﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class World
    {
        public Player player1;
        private static World instance;

        public static World Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new World();
                }
                return instance;
            }
        }
        private World()
        {

        }
        public void Update()
        {
            player1.Move();
            
        }
        public void Draw(Graphics gc)
        {
            player1.DrawPlayer(gc);
            player1.Gun.DrawProjectile(gc);
        }
    }
}
