﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Player
    {
        public Gun Gun = new Gun();
        private Rectangle Hitbox;
        private Point PlayerPosition;
        private int Health, Movement;
        public bool left, right, up, down, Draw = true;
        public Player()
        {
            UpdateHitbox();
        }
        public Point GetPlayerPosition { get { return PlayerPosition; } }
        public void Move()
        {
            if (left)
            {
                PlayerPosition.X -= Movement;
                Draw = true;
            }
            if (right)
            {
                PlayerPosition.X += Movement;
                Draw = true;
            }
            if (up)
            {
                PlayerPosition.Y -= Movement;
                Draw = true;
            }
            if (down)
            {
                PlayerPosition.Y += Movement;
                Draw = true;
            }
            UpdateHitbox();
        }
        public void DrawPlayer(Graphics gc)
        { 
            gc.FillEllipse(Brushes.Black, Hitbox);
            Draw = false;
        }
        private void UpdateHitbox()
        {
            Hitbox = new Rectangle(PlayerPosition, new Size(10, 10));
        }
        public void StandardStats(Point Startposition)
        {
            this.PlayerPosition = Startposition;
            this.Health = 100;
            this.Movement = 4;
        }
    }
}
