﻿namespace Game
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trUpdare = new System.Windows.Forms.Timer(this.components);
            this.pnlLevel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // trUpdare
            // 
            this.trUpdare.Enabled = true;
            this.trUpdare.Interval = 10;
            this.trUpdare.Tick += new System.EventHandler(this.trUpdare_Tick);
            // 
            // pnlLevel
            // 
            this.pnlLevel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLevel.Location = new System.Drawing.Point(0, 0);
            this.pnlLevel.Name = "pnlLevel";
            this.pnlLevel.Size = new System.Drawing.Size(776, 455);
            this.pnlLevel.TabIndex = 0;
            this.pnlLevel.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlLevel_Paint);
            this.pnlLevel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pnlLevel_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 455);
            this.Controls.Add(this.pnlLevel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer trUpdare;
        private System.Windows.Forms.Panel pnlLevel;
    }
}

